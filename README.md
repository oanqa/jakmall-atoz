# Introduction
this is jakmall atoz application using new symfony version,
for more info please visit [symfony-flex](https://github.com/symfony/flex)

# Requirements
```
php >= 7.1
```

# Installation
Install dependency:
```
composer install

```

Create database:
```
cp .env.dist .env
sed -i '/DATABASE_URL/c\DATABASE_URL="mysql://root@127.0.0.1:3306/jakmall-atoz?charset=utf8mb4&serverVersion=5.7"' .env
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
```

# Application Server
to start application server use:
```
bin/console server:start 
```

to stop application use:
```
bin/console server:stop
```

# Reference
* [symfony-flex](https://github.com/symfony/flex)
* [symfony 4: future symfony version](https://symfony.com/blog/symfony-4-a-new-way-to-develop-applications)
* [fosuserbundle: for user management](http://symfony.com/doc/current/bundles/FOSUserBundle/index.html)
* [prooph service bus](https://github.com/prooph/service-bus)
