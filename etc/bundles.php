<?php

return [
    'Symfony\Bundle\FrameworkBundle\FrameworkBundle' => ['all' => true],
    'Symfony\Bundle\WebServerBundle\WebServerBundle' => ['dev' => true],
    'Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle' => ['all' => true],
    'Doctrine\Bundle\DoctrineBundle\DoctrineBundle' => ['all' => true],
    'Symfony\Bundle\TwigBundle\TwigBundle' => ['all' => true],
    'Symfony\Bundle\WebProfilerBundle\WebProfilerBundle' => ['dev' => true, 'test' => true],
    'Symfony\Bundle\MonologBundle\MonologBundle' => ['all' => true],
    'Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle' => ['all' => true],
    'Prooph\Bundle\ServiceBus\ProophServiceBusBundle' => ['all' => true],
    'Symfony\Bundle\DebugBundle\DebugBundle' => ['dev' => true, 'test' => true],
    'Symfony\Bundle\SecurityBundle\SecurityBundle' => ['all' => true],
    'FOS\UserBundle\FOSUserBundle' => ['all' => true],
    'Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle' => ['all' => true],
    'Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle' => ['all' => true],
    'winzou\Bundle\StateMachineBundle\winzouStateMachineBundle' => ['all' => true],
    'WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle' => ['all' => true],
    'FOS\RestBundle\FOSRestBundle' => ['all' => true],
    'Sylius\Bundle\ResourceBundle\SyliusResourceBundle' => ['all' => true],
];
