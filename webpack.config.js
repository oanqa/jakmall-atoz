module.exports = function (env) {
    let path = require('path'),
        webpack = require('webpack'),
        ExtractTextPlugin = require('extract-text-webpack-plugin'),
        resources, dist;

    resources = path.join(__dirname, 'etc', 'resources', 'assets');
    dist = path.join(__dirname, 'web', 'assets');

    return {
        entry: {
            app: path.join(resources, 'js', 'main.js')
        },
        output: {
            path: dist,
            filename: 'js/[name].js'
        },
        plugins: [
            new webpack.NamedModulesPlugin(),
            new ExtractTextPlugin('css/style.css'),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery",
                Tether: "tether",
                "window.Tether": "tether"
            }),
        ],
        module: {
            rules: [
                {
                    test: /\.(jpe?g|png|gif|svg)$/,
                    loader: "url-loader?name=img/[name].[ext]&limit=10000!img-loader"
                },
                {
                    test: /\.(woff2?|ttf|eot|svg|otf)$/,
                    loader: "file-loader?name=fonts/[name].[ext]"
                },
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: ["css-loader", "sass-loader"],
                        publicPath: '../'
                    })
                }
            ]
        }
    };
};