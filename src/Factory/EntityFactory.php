<?php

namespace App\Factory;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class EntityFactory
{
    private $className;

    public function __construct(string $className)
    {
        $this->className = $className;
    }

    public function createNew(UuidInterface $id = null)
    {
        if (null === $id) {
            $id = Uuid::uuid4();
        }

        $entity = new $this->className();
        $setEntityId = function ($entity, UuidInterface $uuid) {
            $entity->id = $uuid;
        };

        $setEntityId = \Closure::bind($setEntityId, null, $entity);
        $setEntityId($entity, $id);

        return $entity;
    }
}