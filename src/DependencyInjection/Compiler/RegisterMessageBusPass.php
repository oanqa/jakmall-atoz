<?php

namespace App\DependencyInjection\Compiler;

use Prooph\Bundle\ServiceBus\DependencyInjection\ProophServiceBusExtension;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class RegisterMessageBusPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        foreach (ProophServiceBusExtension::AVAILABLE_BUSES as $type => $class) {
            if (!$container->hasParameter($parameterBusesName = sprintf('prooph_service_bus.%s_buses', $type))) {
                continue;
            }

            if (!$container->hasParameter($parameterClassName = sprintf('app.%s_bus.class', $type))) {
                continue;
            }

            $busClass = $container->getParameter($parameterClassName);
            foreach ($container->getParameter($parameterBusesName) as $name => $bus) {
                $definitionId = sprintf('app.%s_bus.%s.decorator', $type, $name);

                $definition = new Definition($busClass);
                $definition->setDecoratedService($bus, null, 256);
                $definition->setArguments([new Reference(sprintf('%s.inner', $definitionId))]);
                $definition->setPublic(false);

                $container->setDefinition($definitionId, $definition);
            }
        }
    }
}
