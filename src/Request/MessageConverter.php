<?php

namespace App\Request;

use App\ServiceBus\Message\MessageInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface as SymfonySerializerException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class MessageConverter implements ParamConverterInterface
{
    const REQUEST_FORMAT = 'json';

    private $serializer;
    private $validator;
    private $tokenStorage;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = (array)$configuration->getOptions();
        $context = [];

        try {
            $object = $this->serializer->deserialize(
                $this->getPayload($request),
                $configuration->getClass(),
                self::REQUEST_FORMAT,
                $context
            );
        } catch (SymfonySerializerException $e) {
            return $this->throwException(new BadRequestHttpException($e->getMessage(), $e), $configuration);
        }

        $request->attributes->set($configuration->getName(), $object);

        if (null !== $this->validator) {
            $validatorOptions = $this->getValidatorOptions($options);

            $errors = $this->validator->validate($object, null, $validatorOptions['groups']);

            $request->attributes->set('validator', $errors);
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return null !== $configuration->getClass() && true === $this->isMessage($configuration->getClass());
    }

    private function isMessage(string $class): bool
    {
        $class = new \ReflectionClass($class);

        if ($class->implementsInterface(MessageInterface::class)) {
            return true;
        }

        return false;
    }

    private function throwException(\Exception $exception, ParamConverter $configuration)
    {
        if ($configuration->isOptional()) {
            return false;
        }

        throw $exception;
    }

    private function getPayload(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $userPayload = $user ? ['userId' => $user->getId()] : [];
        $payload = array_merge(
            $request->attributes->all(),
            $request->query->all(),
            $request->request->all(),
            $userPayload
        );

        return json_encode($payload);
    }

    private function getValidatorOptions(array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'groups' => null,
                'traverse' => false,
                'deep' => false,
            ]
        );

        return $resolver->resolve(isset($options['validator']) ? $options['validator'] : []);
    }
}