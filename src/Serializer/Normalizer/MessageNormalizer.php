<?php

namespace App\Serializer\Normalizer;

use App\ServiceBus\Message\AbstractMessage;
use App\ServiceBus\Message\MessageHasResultInterface;
use App\ServiceBus\Message\MessageInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class MessageNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var PropertyNormalizer
     */
    private $normalizer;

    /**
     * @var array
     */
    private $ignoredCache = [];

    /**
     * Constructor.
     *
     * @param PropertyNormalizer $normalizer
     */
    public function __construct(PropertyNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $this->supports(get_class($data));
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $context = $this->getContext(get_class($object), $context);

        if (!empty($payload = $object->payload())) {
            return $payload;
        }

        return $this->getNormalizer($context)->normalize($object, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $this->supports($type);
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $context = $this->getContext($class, $context);

        return $this->getNormalizer($context)->denormalize($data, $class, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->normalizer->setSerializer($serializer);
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    private function supports($class)
    {
        $class = new \ReflectionClass($class);

        if ($class->implementsInterface(MessageInterface::class)) {
            return true;
        }

        return false;
    }

    /**
     * @param array $context
     *
     * @return PropertyNormalizer
     */
    private function getNormalizer(array $context)
    {
        //reset ignored attributes.
        $this->normalizer->setIgnoredAttributes([]);

        if (isset($context['ignored']) && $ignored = $context['ignored']) {
            $this->normalizer->setIgnoredAttributes($ignored);
        }

        return $this->normalizer;
    }

    /**
     * @param string $class
     * @param array  $context
     *
     * @return array
     */
    private function getContext($class, array $context)
    {
        $context['ignored'] = $this->getIgnoredAttributes($class, $context);

        return $context;
    }

    /**
     * @param string $class
     * @param array  $context
     *
     * @return array
     */
    private function getIgnoredAttributes($class, array $context)
    {
        if (isset($this->ignoredCache[$class])) {
            return $this->ignoredCache[$class];
        }

        $ignored = [];
        $reflection = new \ReflectionClass(AbstractMessage::class);
        foreach ($reflection->getProperties() as $property) {
            $ignored[] = $property->getName();
        }

        $reflection = new \ReflectionClass($class);
        if ($reflection->implementsInterface(MessageHasResultInterface::class)) {
            $ignored[] = 'result';
        }

        $ignored = array_merge(isset($context['ignored']) ? $context['ignored'] : [], $ignored);

        return $this->ignoredCache[$class] = $ignored;
    }
}