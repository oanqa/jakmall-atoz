<?php

namespace App\Handler\Query;

use App\Message\Query\FindOrderByNumberQuery;
use App\Repository\OrderRepository;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class FindOrderByNumberQueryHandler
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function find(FindOrderByNumberQuery $query)
    {
        return $this->repository->findOneBy(['number' => $query->get('orderNumber')]);
    }
}