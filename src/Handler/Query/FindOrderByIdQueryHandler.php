<?php

namespace App\Handler\Query;

use App\Message\Query\FindOrderByIdQuery;
use App\Repository\OrderRepository;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class FindOrderByIdQueryHandler
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function find(FindOrderByIdQuery $query)
    {
        return $this->repository->find($query->get('id'));
    }
}