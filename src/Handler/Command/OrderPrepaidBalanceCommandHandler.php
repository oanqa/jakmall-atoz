<?php

namespace App\Handler\Command;

use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\OrderNumberGeneratorInterface;
use App\Entity\OrderPrepaidBalanceMeta;
use App\Factory\EntityFactory;
use App\Message\Command\OrderPrepaidBalanceCommand;
use App\Repository\OrderRepository;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderPrepaidBalanceCommandHandler
{
    private $generator;
    private $repository;
    private $tokenStorage;

    public function __construct(OrderNumberGeneratorInterface $generator, OrderRepository $repository, TokenStorageInterface $tokenStorage)
    {
        $this->generator = $generator;
        $this->repository = $repository;
        $this->tokenStorage = $tokenStorage;
    }

    public function handle(OrderPrepaidBalanceCommand $command): OrderInterface
    {
        $factory = new EntityFactory(Order::class);

        /** @var OrderInterface $order */
        $order = $factory->createNew();
        $order->setNumber($this->generator->generate());
        $order->setType(OrderInterface::TYPE_PREPAID_BALANCE);
        $order->setTotal($this->calculateTotal($command->get('value')));
        $order->setUser($this->getUser());

        //TODO::use helper/serializer to generate meta!!
        $order->setMeta(new OrderPrepaidBalanceMeta($command->get('mobilePhone'), $command->get('value')));

        //TODO::implement state machine here!!
        $order->setState(OrderInterface::STATE_NEW);

        $this->repository->add($order);

        return $order;
    }

    private function getUser(): UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function calculateTotal(int $value): int
    {
        //TODO::make processor service.
        return $value * 1.05;
    }
}