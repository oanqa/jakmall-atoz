<?php

namespace App\Handler\Command;

use App\Entity\OrderInterface;
use App\Entity\OrderProductCommerceMeta;
use App\Message\Command\PayOrderCommand;
use App\Message\Query\FindOrderByNumberQuery;
use App\Repository\OrderRepository;
use App\ServiceBus\CompositeMessageBus;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class PayOrderCommandHandler
{
    private $repository;
    private $bus;
    private $serializer;
    private $tokenStorage;

    public function __construct(OrderRepository $repository, CompositeMessageBus $bus, SerializerInterface $serializer, TokenStorageInterface $tokenStorage)
    {
        $this->repository = $repository;
        $this->bus = $bus;
        $this->serializer = $serializer;
        $this->tokenStorage = $tokenStorage;
    }

    public function handle(PayOrderCommand $command): OrderInterface
    {
        $query = $this->serializer->denormalize(
            ['orderNumber' => $command->get('orderNumber')],
            FindOrderByNumberQuery::class
        );
        /** @var OrderInterface $order */
        $order = $this->bus->dispatch($query);

        if (!$this->validate($order)) {
            return $order;
        }

        if ($this->isExpired($order)) {
            $order->setState(OrderInterface::STATE_CANCEL);
        } else {
            $this->process($order);
        }

        $this->repository->add($order);

        return $order;
    }

    private function process(OrderInterface $order)
    {
        //TODO::create service processor for each order type.
        //each of order type can be have own processor and own validation.
        $order->setState(OrderInterface::STATE_SUCCESS);

        if ($this->isPeakTime() && OrderInterface::TYPE_PREPAID_BALANCE === $order->getType()) {
            $order->setState(OrderInterface::STATE_FAIL);
        }

        if (OrderInterface::TYPE_PRODUCT === $order->getType()) {
            //create new meta, because doctrine value object is immutable!!
            $meta = new OrderProductCommerceMeta(
                $order->getMeta()->getProduct(),
                $order->getMeta()->getShippingAddress(),
                $order->getMeta()->getPrice(),
                $this->generateShippingCode()
            );

            $order->setMeta($meta);
        }
    }

    private function validate(OrderInterface $order): bool
    {
        if (
            OrderInterface::STATE_CANCEL === $order->getState() ||
            OrderInterface::STATE_SUCCESS === $order->getState()
        ) {
            return false;
        }

        if ($this->getUser() !== $order->getUser()) {
            return false;
        }

        return true;
    }

    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function isExpired(OrderInterface $order): bool
    {
        $now = new \DateTime();
        $expired = $order->getCreatedAt()->modify('5 minute');

        if ($now > $expired) {
            return true;
        }

        return false;
    }

    private function isPeakTime()
    {
        $now = new \DateTime();
        $hour = $now->format('G');
        $probability = ($hour >= 9 && $hour <= 17) ? 90 : 40;

        if (rand(1, 100) <= $probability) {
            return true;
        }

        return false;
    }

    private function generateShippingCode(): string
    {
        $code = bin2hex(random_bytes(8));

        return substr($code, 0, 8);
    }
}