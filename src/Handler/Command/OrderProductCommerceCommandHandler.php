<?php

namespace App\Handler\Command;

use App\Entity\Order;
use App\Entity\OrderInterface;
use App\Entity\OrderNumberGeneratorInterface;
use App\Entity\OrderProductCommerceMeta;
use App\Factory\EntityFactory;
use App\Message\Command\OrderProductCommerceCommand;
use App\Repository\OrderRepository;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderProductCommerceCommandHandler
{
    private $generator;
    private $repository;
    private $tokenStorage;

    public function __construct(OrderNumberGeneratorInterface $generator, OrderRepository $repository, TokenStorageInterface $tokenStorage)
    {
        $this->generator = $generator;
        $this->repository = $repository;
        $this->tokenStorage = $tokenStorage;
    }

    public function handle(OrderProductCommerceCommand $command): OrderInterface
    {
        $factory = new EntityFactory(Order::class);

        /** @var OrderInterface $order */
        $order = $factory->createNew();
        $order->setNumber($this->generator->generate());
        $order->setType(OrderInterface::TYPE_PRODUCT);
        $order->setTotal($this->calculateTotal($command->get('price')));
        $order->setUser($this->getUser());

        //TODO::use helper/serializer to generate meta!!
        $order->setMeta(
            new OrderProductCommerceMeta(
                $command->get('product'),
                $command->get('shippingAddress'),
                $command->get('price')
            )
        );

        //TODO::implement state machine here!!
        $order->setState(OrderInterface::STATE_NEW);

        $this->repository->add($order);

        return $order;
    }

    private function getUser(): UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    private function calculateTotal(int $price): int
    {
        //TODO::make processor service.
        return $price + 10;
    }
}