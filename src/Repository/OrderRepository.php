<?php

namespace App\Repository;

use FOS\UserBundle\Model\UserInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderRepository extends EntityRepository
{
    public function findAllByUser(UserInterface $user)
    {
        $criteria = ['user' => $user];

        return $this->createPaginator($criteria, ['createdAt' => 'desc']);
    }
}