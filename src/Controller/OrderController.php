<?php

namespace App\Controller;

use App\Message\Command\OrderPrepaidBalanceCommand;
use App\Message\Command\OrderProductCommerceCommand;
use App\Message\Command\PayOrderCommand;
use App\Message\Query\FindOrderByIdQuery;
use App\Message\Query\FindOrderByNumberQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class OrderController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('app.order.index');
    }

    /**
     * @Route("/prepaid-balance", name="app.order.prepaid_balance_get")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function prepaidBalanceAction(Request $request): Response
    {
        $this->checkSession($request);

        return $this->render('order/prepaid-balance.html.twig');
    }

    /**
     * @Route("/prepaid-balance", name="app.order.prepaid_balance_post")
     * @Method("POST")
     * @ParamConverter("message", converter="message_converter")
     *
     * @param OrderPrepaidBalanceCommand       $message
     * @param ConstraintViolationListInterface $validator
     *
     * @return Response
     */
    public function prepaidBalanceSubmitAction(
        OrderPrepaidBalanceCommand $message,
        ConstraintViolationListInterface $validator
    ): Response {
        if (count($validator) > 0) {
            $this->flashError($validator);

            return $this->redirectToRoute('app.order.prepaid_balance_get');
        }

        $order = $this->get('app_buses')->dispatch($message);

        return $this->redirectToRoute('app.order.success', ['id' => $order->getId()]);
    }

    /**
     * @Route("/product", name="app.order.product_commerce_get")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function productCommerceAction(Request $request): Response
    {
        $this->checkSession($request);

        return $this->render('order/product-commerce.html.twig');
    }

    /**
     * @Route("/product", name="app.order.product_commerce_post")
     * @Method("POST")
     * @ParamConverter("message", converter="message_converter")
     *
     * @param OrderProductCommerceCommand      $message
     * @param ConstraintViolationListInterface $validator
     *
     * @return Response
     */
    public function productCommerceSubmitAction(
        OrderProductCommerceCommand $message,
        ConstraintViolationListInterface $validator
    ): Response {
        if (count($validator) > 0) {
            $this->flashError($validator);

            return $this->redirectToRoute('app.order.product_commerce_get');
        }

        $order = $this->get('app_buses')->dispatch($message);

        return $this->redirectToRoute('app.order.success', ['id' => $order->getId()]);
    }

    /**
     * @Route("/payment/{id}", name="app.order.payment_get", defaults={"id"=null})
     * @Method("GET")
     *
     * @param Request     $request
     * @param string|null $id
     *
     * @return Response
     */
    public function paymentAction(Request $request, string $id = null): Response
    {
        $this->checkSession($request);

        $orderNumber = null;
        if (null !== $id) {
            $query = $this->get('serializer')->denormalize(['id' => $id], FindOrderByIdQuery::class);
            $order = $this->get('app_buses')->dispatch($query);
            $orderNumber = $order->getNumber();
        }

        return $this->render('order/payment.html.twig', ['orderNumber' => $orderNumber]);
    }

    /**
     * @Route("/payment/{id}", name="app.order.payment_post", defaults={"id"=null})
     * @Method("POST")
     * @ParamConverter("message", converter="message_converter")
     *
     * @param PayOrderCommand                  $message
     * @param ConstraintViolationListInterface $validator
     * @param string|null                      $id
     *
     * @return Response
     */
    public function paymentSubmitAction(
        PayOrderCommand $message,
        ConstraintViolationListInterface $validator,
        string $id = null
    ) {
        if (count($validator) > 0) {
            $this->flashError($validator);

            return $this->redirectToRoute('app.order.payment_get', ['id' => $id]);
        }

        $query = $this->get('serializer')->denormalize(
            ['orderNumber' => $message->get('orderNumber')],
            FindOrderByNumberQuery::class
        )
        ;

        if (null === $order = $this->get('app_buses')->dispatch($query)) {
            $this->addFlash('error', 'Order not found');

            return $this->redirectToRoute('app.order.payment_get', ['id' => $id]);
        }

        $this->get('app_buses')->dispatch($message);

        return $this->redirectToRoute('app.order.index');
    }

    /**
     * @Route("/success/{id}", name="app.order.success")
     * @Method("GET")
     * @ParamConverter("query", converter="message_converter")
     *
     * @param FindOrderByIdQuery $query
     *
     * @return Response
     */
    public function successAction(FindOrderByIdQuery $query): Response
    {
        $order = $this->get('app_buses')->dispatch($query);

        return $this->render('order/success.html.twig', ['order' => $order]);
    }

    private function checkSession(Request $request)
    {
        if ($request->hasPreviousSession()) {
            $request->getSession()->start();
        }
    }

    private function flashError(ConstraintViolationListInterface $validator): void
    {
        foreach ($validator as $violation) {
            $this->addFlash(sprintf('error.%s', $violation->getPropertyPath()), $violation->getMessage());
        }
    }
}