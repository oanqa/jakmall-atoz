<?php

namespace App\Message\Query;

use App\ServiceBus\Message\AbstractQuery;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class FindOrderByIdQuery extends AbstractQuery
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    protected $id;
}