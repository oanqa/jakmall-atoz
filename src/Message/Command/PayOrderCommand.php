<?php

namespace App\Message\Command;

use App\ServiceBus\Message\AbstractCommand;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class PayOrderCommand extends AbstractCommand
{
    /**
     * @Assert\NotBlank()
     */
    protected $orderNumber;
}