<?php

namespace App\Message\Command;

use App\ServiceBus\Message\AbstractCommand;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderProductCommerceCommand extends AbstractCommand
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="150")
     */
    protected $product;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="150")
     */
    protected $shippingAddress;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    protected $price;

    /**
     * @Assert\NotBlank()
     */
    protected $userId;
}