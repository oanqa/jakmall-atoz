<?php

namespace App\Message\Command;

use App\ServiceBus\Message\AbstractCommand;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderPrepaidBalanceCommand extends AbstractCommand
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\Length(min="7", max="12")
     * @Assert\Regex(
     *     pattern="/^081\d+/",
     *     message="This value is not valid, Must be prefixed with 081"
     * )
     */
    protected $mobilePhone;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    protected $value;

    /**
     * @Assert\NotBlank()
     */
    protected $userId;
}