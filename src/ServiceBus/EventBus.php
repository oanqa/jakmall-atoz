<?php

namespace App\ServiceBus;

use App\ServiceBus\Message\MessageInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class EventBus extends AbstractMessageBus implements MessageBusSupportsInterface
{
    /**
     * {@inheritdoc}
     */
    public function dispatch($event)
    {
        $this->messageBus->dispatch($event);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($message)
    {
        return $message instanceof MessageInterface && MessageInterface::TYPE_EVENT === $message->messageType();
    }
}
