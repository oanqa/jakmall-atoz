<?php

namespace App\ServiceBus;

use Prooph\ServiceBus\MessageBus as BaseMessageBus;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
abstract class AbstractMessageBus implements MessageBusInterface
{
    public const EVENT_PARAM_MESSAGE_RESULT = 'message-result';

    /**
     * @var BaseMessageBus
     */
    protected $messageBus;

    /**
     * Constructor.
     *
     * @param BaseMessageBus $messageBus
     */
    public function __construct(BaseMessageBus $messageBus)
    {
        $this->messageBus = $messageBus;
    }
}
