<?php

namespace App\ServiceBus;

use App\ServiceBus\Message\MessageHasResultInterface;
use App\ServiceBus\Message\MessageInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class CommandBus extends AbstractMessageBus implements MessageBusSupportsInterface
{
    /**
     * {@inheritdoc}
     */
    public function dispatch($command)
    {
        $this->messageBus->dispatch($command);

        if ($command instanceof MessageHasResultInterface) {
            return $command->getResult();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($message)
    {
        return $message instanceof MessageInterface && MessageInterface::TYPE_COMMAND === $message->messageType();
    }
}
