<?php

namespace App\ServiceBus\Message;

use Prooph\Common\Messaging\Message;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
interface MessageInterface extends Message
{
}