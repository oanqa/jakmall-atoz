<?php

namespace App\ServiceBus\Message;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
interface MessageHasResultInterface
{
    public function setResult($result);

    public function getResult();
}
