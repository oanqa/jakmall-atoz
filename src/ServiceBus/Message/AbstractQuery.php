<?php

namespace App\ServiceBus\Message;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
abstract class AbstractQuery extends AbstractMessage implements MessageHasResultInterface
{
    use MessageHasResultTrait;

    /**
     * @return string
     */
    public function messageType(): string
    {
        return self::TYPE_QUERY;
    }
}