<?php

namespace App\ServiceBus\Message;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
trait MessageHasResultTrait
{
    /**
     * @var mixed
     */
    private $result;

    /**
     * @param mixed $result
     */
    public function setResult($result) {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        return $this->result;
    }
}
