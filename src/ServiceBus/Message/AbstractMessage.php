<?php

namespace App\ServiceBus\Message;

use Prooph\Common\Messaging\DomainMessage;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
abstract class AbstractMessage extends DomainMessage implements MessageInterface
{
    /**
     * @var array
     */
    protected $payload = [];

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * @return array
     */
    public function payload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @param string $property
     *
     * @return mixed
     */
    public function get($property)
    {
        if (property_exists($this, $property)) {
            return $this->{$property};
        }

        throw new \InvalidArgumentException(
            sprintf('Property "%s" was not found in "%s"', $property, get_class($this))
        );
    }
}