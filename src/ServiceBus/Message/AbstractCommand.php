<?php

namespace App\ServiceBus\Message;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
abstract class AbstractCommand extends AbstractMessage implements MessageHasResultInterface
{
    use MessageHasResultTrait;

    /**
     * {@inheritdoc}
     */
    public function messageType(): string
    {
        return self::TYPE_COMMAND;
    }
}
