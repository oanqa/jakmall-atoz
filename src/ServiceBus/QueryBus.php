<?php

namespace App\ServiceBus;

use App\ServiceBus\Message\MessageHasResultInterface;
use App\ServiceBus\Message\MessageInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class QueryBus extends AbstractMessageBus implements MessageBusSupportsInterface
{
    /**
     * {@inheritdoc}
     */
    public function dispatch($query)
    {
        $this->messageBus->dispatch($query);

        if ($query instanceof MessageHasResultInterface) {
            return $query->getResult();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($message)
    {
        return $message instanceof MessageInterface && MessageInterface::TYPE_QUERY === $message->messageType();
    }
}
