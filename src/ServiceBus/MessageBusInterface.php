<?php

namespace App\ServiceBus;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
interface MessageBusInterface
{
    /**
     * @param mixed $message
     *
     * @return mixed
     */
    public function dispatch($message);
}