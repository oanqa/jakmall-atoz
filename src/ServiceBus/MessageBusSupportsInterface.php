<?php

namespace App\ServiceBus;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
interface MessageBusSupportsInterface extends MessageBusInterface
{
    /**
     * @param mixed $message
     *
     * @return bool
     */
    public function supports($message);
}