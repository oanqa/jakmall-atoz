<?php

namespace App\ServiceBus;

use Zend\Stdlib\PriorityQueue;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class CompositeMessageBus implements MessageBusInterface
{
    /**
     * @var PriorityQueue
     */
    private $buses;

    public function __construct(array $buses = [])
    {
        $this->buses = new PriorityQueue();

        foreach ($buses as $bus) {
            $this->addBus($bus);
        }
    }

    public function addBus(MessageBusSupportsInterface $bus, $priority = 0): void
    {
        $this->buses->insert($bus, $priority);
    }
    
    /**
     * {@inheritdoc}
     */
    public function dispatch($message)
    {
        /** @var MessageBusSupportsInterface $bus */
        foreach ($this->buses as $bus) {
            if ($bus->supports($message)) {
                return $bus->dispatch($message);
            }
        }

        throw new \RuntimeException(sprintf('Cannot find bus for this message "%s"', get_class($message)));
    }
}