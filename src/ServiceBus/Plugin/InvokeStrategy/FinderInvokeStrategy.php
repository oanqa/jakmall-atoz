<?php

namespace App\ServiceBus\Plugin\InvokeStrategy;

use App\ServiceBus\AbstractMessageBus;
use App\ServiceBus\Message\MessageHasResultInterface;
use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\AbstractPlugin;
use Prooph\ServiceBus\QueryBus;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class FinderInvokeStrategy extends AbstractPlugin
{
    public function attachToMessageBus(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            QueryBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent): void {
                if ($actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED, false)) {
                    return;
                }
                $finder = $actionEvent->getParam(QueryBus::EVENT_PARAM_MESSAGE_HANDLER);
                $query = $actionEvent->getParam(QueryBus::EVENT_PARAM_MESSAGE);
                $deferred = $actionEvent->getParam(QueryBus::EVENT_PARAM_DEFERRED);
                if (is_object($finder)) {
                    $result = $finder->find($query, $deferred);
                    $actionEvent->setParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED, true);

                    if ($query instanceof MessageHasResultInterface && $result) {
                        $actionEvent->setParam(AbstractMessageBus::EVENT_PARAM_MESSAGE_RESULT, $result);
                    }
                }
            },
            QueryBus::PRIORITY_INVOKE_HANDLER
        );

        $this->listenerHandlers[] = $messageBus->attach(
            MessageBus::EVENT_FINALIZE,
            function (ActionEvent $actionEvent): void {
                $message = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE);
                $result = $actionEvent->getParam(AbstractMessageBus::EVENT_PARAM_MESSAGE_RESULT);

                if ($message instanceof MessageHasResultInterface && $result) {
                    $message->setResult($result);
                }
            }
        );
    }
}