<?php

namespace App\ServiceBus\Plugin\InvokeStrategy;

use App\ServiceBus\AbstractMessageBus;
use App\ServiceBus\Message\MessageHasResultInterface;
use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\AbstractPlugin;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class HandleResultCommandStrategy extends AbstractPlugin
{
    public function attachToMessageBus(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent): void {
                $message = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE);
                $handler = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLER);

                $result = $handler->handle($message);
                $actionEvent->setParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED, true);

                if ($message instanceof MessageHasResultInterface && $result) {
                    $actionEvent->setParam(AbstractMessageBus::EVENT_PARAM_MESSAGE_RESULT, $result);
                }
            },
            MessageBus::PRIORITY_INVOKE_HANDLER
        );

        $this->listenerHandlers[] = $messageBus->attach(
            MessageBus::EVENT_FINALIZE,
            function (ActionEvent $actionEvent): void {
                $message = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE);
                $result = $actionEvent->getParam(AbstractMessageBus::EVENT_PARAM_MESSAGE_RESULT);

                if ($message instanceof MessageHasResultInterface && $result) {
                    $message->setResult($result);
                }
            }
        );
    }
}
