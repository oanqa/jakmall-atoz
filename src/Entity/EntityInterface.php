<?php

namespace App\Entity;

use Ramsey\Uuid\UuidInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
interface EntityInterface extends ResourceInterface
{
    public function getId(): UuidInterface;
}