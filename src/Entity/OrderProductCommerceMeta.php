<?php

namespace App\Entity;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderProductCommerceMeta implements OrderMetaInterface
{
    protected $product;
    protected $shippingAddress;
    protected $price;
    protected $shippingCode;

    public function __construct(string $product, string $shippingAddress, int $price, string $shippingCode = null)
    {
        $this->product = $product;
        $this->shippingAddress = $shippingAddress;
        $this->price = $price;
        $this->shippingCode = $shippingCode;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getShippingAddress(): string
    {
        return $this->shippingAddress;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getShippingCode(): string
    {
        return $this->shippingCode;
    }
}