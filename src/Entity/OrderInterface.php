<?php

namespace App\Entity;

use FOS\UserBundle\Model\UserInterface;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
interface OrderInterface extends EntityInterface
{
    const TYPE_PREPAID_BALANCE = 'prepaid-balance';
    const TYPE_PRODUCT = 'product';

    const STATE_NEW = 'new';
    const STATE_SUCCESS = 'success';
    const STATE_CANCEL = 'cancel';
    const STATE_FAIL = 'fail';

    public function getState(): string;

    public function setState(string $state);

    public function getNumber(): int;

    public function setNumber(int $orderNumber);

    public function getType(): string;

    public function setType(string $type);

    public function getTotal(): int;

    public function setTotal(int $total);

    public function getMeta(): OrderMetaInterface;

    public function setMeta(OrderMetaInterface $meta);

    public function getUser(): UserInterface;

    public function setUser(UserInterface $user);

    public function getCreatedAt(): \DateTime;

    public function setCreatedAt(\DateTime $createdAt);

    public function getUpdatedAt(): \DateTime;

    public function setUpdatedAt(\DateTime $updatedAt);
}