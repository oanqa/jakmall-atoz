<?php

namespace App\Entity;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderNumberGenerator implements OrderNumberGeneratorInterface
{
    public function generate()
    {
        //simple number generator!!
        return random_int(1000000000, 9999999999);
    }
}