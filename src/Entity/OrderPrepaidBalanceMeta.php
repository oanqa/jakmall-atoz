<?php

namespace App\Entity;

/**
 * @author      Johan Tanaka <tanaka.johan@gmail.com>
 * @author      Andriani Suastiyu <fanx.andriani@gmail.com>
 */
class OrderPrepaidBalanceMeta implements OrderMetaInterface
{
    protected $mobilePhone;
    protected $value;

    public function __construct(string $mobilePhone, int $value)
    {
        $this->mobilePhone = $mobilePhone;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getMobilePhone(): string
    {
        return $this->mobilePhone;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}